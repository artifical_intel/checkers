import java.sql.Array;
import java.util.*;



public class Player {
	/**
     * Performs a move
     *
     * @param pState
     *            the current state of the board
     * @param pDue
     *            time before which we must have returned
     * @return the next state the board is in after our move
     */
	
	
	private int myPlayer;
	private int opponent;
	
	long startTime;
	
	public HashMap<Integer, ArrayList<GameState>> unsortedMoves; 
    private HashMap<GameState, Integer> visitedStates = new HashMap<GameState, Integer>();

    
    public GameState play(final GameState pState, final Deadline pDue) {

        Vector<GameState> lNextStates = new Vector<GameState>();
        pState.findPossibleMoves(lNextStates);

        if (lNextStates.size() == 0) {
            // Must play "pass" move if there are no other moves possible.
            return new GameState(pState, new Move());
        }

        /**
         * Here you should write your algorithms to get the best next move, i.e.
         * the best next state. This skeleton returns a random move instead.
         */
        
        myPlayer = pState.getNextPlayer();
        
        if (myPlayer == Constants.CELL_RED) {
        	opponent = Constants.CELL_WHITE;
        }else {
        	opponent = Constants.CELL_RED;
        }
        
        return iterativeDeepening(pState, pDue);
    }
        
        
      
    
    public GameState iterativeDeepening(GameState pState, Deadline pDue) {
    	
        Vector<GameState> lNextStates = new Vector<GameState>();
        pState.findPossibleMoves(lNextStates);
    	
    	int score = 0;
        GameState bestMove = null;
        boolean keepGoing = true;
        
        

		    for (int depth= 0; depth < Integer.MAX_VALUE; depth++) {
		    	
		    	if (pDue.timeUntil() < 600000000 | keepGoing == false) {
		    		return bestMove;
		    	}
		    	else {
		    		//System.err.println(depth);
			    	int v = Integer.MIN_VALUE;          
			        
			        for (GameState child : lNextStates) {
			        	
			        	if(visitedStates.get(child) == null) {
			        					        		
			        		int value =  gamma(pState);
			        		visitedStates.put(child, value);
			        	}
			        	
			        	score = alphaBeta(child, Integer.MIN_VALUE, Integer.MAX_VALUE, depth, opponent);
			        	
			        	if (score > v) {
			        		v = score;
			        		bestMove = child;
			        		
			        	}
			        	
			        	if (score == 1000000 || score == -1000000) {
			        		keepGoing = false;
			        		return bestMove;
			        	}
	
			        }
			        		        
		    }    
        }
        
        
                
    	return bestMove;
    }
    
    public int alphaBeta(GameState pState, int alpha, int beta, int depth, int player) {
    	
    	// State: the current state we are analysing
    	// alpha: the current best value achievable by A
    	// beta : the  current best value achievable by B
    	// player : the current player
    	
    	
    	
        Vector<GameState> lNextStates = new Vector<GameState>();
        pState.findPossibleMoves(lNextStates);
        
        if (lNextStates.size() == 0) {
        	
        	if(player == myPlayer ) {
        		
        		return 1000000;
        	}
        	else {
        		return -1000000;
        	}
        	
        }
        
        else if (depth <= 0 ) {
    		
        	if(visitedStates.get(pState) != null){
        		int value = visitedStates.get(pState);
        		System.err.println(value);
        	    return value;
        	}
        	
    		return gamma(pState);
    		
    	}
    	
	    	else {
	    		if (player == myPlayer) {
	    			
	    			return max_value(pState, alpha, beta, depth, opponent);
	    			
	    		}
	    		
	    		else {
	  
	    			return min_value(pState, alpha, beta, depth, myPlayer);
	    			
	    		}
	    		
	    	}
    	
    }
 
    public int markScore(GameState pState, int player) {
    	
    	int[] weights = 
    		{ 4, 4, 4, 4, 
		        4, 3, 3, 3, 
		         3, 2, 2, 4, 
		        4, 2, 1, 3, 
		         3, 1, 2, 4, 
		        4, 2, 2, 3, 
		         3, 3, 3, 4, 
		        4, 4, 4, 4};
    	
		    	
    	int score = 0;
    	

    	for (int cell = 0; cell < pState.NUMBER_OF_SQUARES; cell++) {
    		    		
    		int valueInCell = pState.get(cell);
    		
    		if ( valueInCell == player) {
    			score += 3; 
    		}
    		
    		else if (valueInCell == player+4) {
    			score += 9;
    		}
    		
    	}	
    	
    	return score;
	
    }
    
     
    public int gamma(GameState pState) {
    	
    	
    	int redMarkScore = markScore(pState, Constants.CELL_RED);
    	int whiteMarkScore = markScore(pState, Constants.CELL_WHITE);
    	
    	int locationScore = locationScore(pState);
    	
    	if(pState.isRedWin()) {
			return 100000;
			
		}else if (pState.isWhiteWin()){
			return -100000;
			
		}
    	
    	if (myPlayer == 1) {
    		
    		
    		int totScore = redMarkScore - whiteMarkScore;
    		return totScore + locationScore;
    	}
    	else {
    		    		
    		int result = whiteMarkScore-redMarkScore;
    		return result + locationScore;
    	}
    	
		    	
    	
    }
       
    public int max_value(GameState pState, int alpha, int beta, int depth, int player) {
    	    	
    	int v =  Integer.MIN_VALUE;
    	
    	unsortedMoves = new HashMap<Integer, ArrayList<GameState>>();
    	
    	Vector<GameState> lNextStates = new Vector<GameState>();
        pState.findPossibleMoves(lNextStates);
        
        /*for (GameState child : lNextStates) {
        	
			v = Math.max(v, alphaBeta(child, alpha, beta, depth-1, child.getNextPlayer()));
			
			alpha = Math.max(alpha,v);

			if (beta <= alpha) {
				break;
				
			}
        	
        }
        */
        
        
        for (GameState child: lNextStates) {
        	
        	int score = gamma(pState);
        	if (unsortedMoves.get(score) == null) {
        		unsortedMoves.put(score, new ArrayList<GameState>());
        	}
        	
        	unsortedMoves.get(score).add(child);
        	
        	if (visitedStates.size()< 1000) {
        		
        		if (visitedStates.get(child) != null) {
        		
        			visitedStates.put(child, score);
        		}
        	}
        }
        
        Object[] sortedMoves = unsortedMoves.keySet().toArray();
        Arrays.sort(sortedMoves);
        
		for (Object key : sortedMoves) {
			
			ArrayList<GameState> childList = unsortedMoves.get(key);
				
			Iterator<GameState> childListIterator = childList.iterator();
			
			while (childListIterator.hasNext()) {
			
				v = Math.max(v, alphaBeta(childListIterator.next(), alpha, beta, depth-1, childListIterator.next().getNextPlayer()));
				
				alpha = Math.max(alpha,v);

				if (beta <= alpha) {
					break;
					
				}
				

			}
			

			
		}
		
    	
		return v;
    	
    }

    public int min_value(GameState pState, int alpha, int beta, int depth, int player) {
    	
    	
    	
    	unsortedMoves = new HashMap<Integer, ArrayList<GameState>>();
    	
    	Vector<GameState> lNextStates = new Vector<GameState>();
        pState.findPossibleMoves(lNextStates);
        
        /*
         * int v = Integer.MAX_VALUE;
        for (GameState child: lNextStates) {
			v = Math.min(v, alphaBeta(child, alpha, beta, depth-1, child.getNextPlayer()));
			
			beta = Math.min(beta, v);
			
			if (beta <= alpha) {
				break;
			}
			
        	
        }
        */
        
        
        
        for (GameState child: lNextStates) {
        	
        	int score = gamma(pState);
        	if (unsortedMoves.get(score) == null) {
        		unsortedMoves.put(score, new ArrayList<GameState>());
        	}
        	
        	unsortedMoves.get(score).add(child);
        	
        	if (visitedStates.size()< 1000) {
        		
        		if (visitedStates.get(child) != null) {
        		
        			visitedStates.put(child, score);
        		}
        	}
        	
        }
        
        Object[] sortedMoves = unsortedMoves.keySet().toArray();
        Arrays.sort(sortedMoves, Collections.reverseOrder());
    	
		int v = Integer.MAX_VALUE;
		
		for (Object key : sortedMoves) {
			
			ArrayList<GameState> childList = unsortedMoves.get(key);
				
			Iterator<GameState> childListIterator = childList.iterator();
		
			while (childListIterator.hasNext()) {
				
				v = Math.min(v, alphaBeta(childListIterator.next(), alpha, beta, depth-1, childListIterator.next().getNextPlayer()));
				
				beta = Math.min(beta, v);
				
				if (beta <= alpha) {
					break;
				}
				
				
			}

			
		}
    	
		return v;
    	
    }
    

   public int locationScore(GameState pState) {
    	
    	//2) Marks in Opponents area = 2p
    	//	 Marks in our bottomline = 0.5p
    	//	 Marks on the edge = 2p
    	
    	int whiteLocationScore = 0;
    	int redLocationScore = 0;
    	

    	// 
		for (int i = 0; i < 4; i ++) {
			

        	int valueInCell = pState.get(i);
        	
        	if (valueInCell == Constants.CELL_WHITE || valueInCell == 6) {
		
        		whiteLocationScore += 5;
        		
        	}
		}
		
		//Block
		for (int i = 28; i < 32; i++) {
			
        	int valueInCell = pState.get(i);
        	
        	if (valueInCell == Constants.CELL_WHITE || valueInCell == 6) {
		
        		whiteLocationScore += 1;
        		
        	}
        
		}
		
		//kanter
		for (int row = 0; row < 8; row++) {
		
			for (int col = 0; col < 8 ; col = col+7 ) {
	        	int valueInCell = pState.get(row, col);
	        	
	        	if (valueInCell == Constants.CELL_WHITE | valueInCell == 6) {
    		
	        		whiteLocationScore += 3;
        		
	        	}
			}

		}
    	

    		
		for (int i = 28; i < 32; i ++) {
			

        	int valueInCell = pState.get(i);
        	
        	if (valueInCell == Constants.CELL_RED || valueInCell == 5) {
		
        		redLocationScore += 5;
        		
        	}
		}
		
		for (int i = 0; i < 4; i++) {
			
        	int valueInCell = pState.get(i);
        	
        	if (valueInCell == Constants.CELL_RED || valueInCell == 5) {
		
        		redLocationScore += 1;
        		
        	}
        
		}
		
		
		for (int row = 0; row < 8; row++) {
		
			for (int col = 0; col < 8 ; col = col+7 ) {
	        	int valueInCell = pState.get(row, col);
	        	
	        	if (valueInCell == Constants.CELL_RED || valueInCell == 5) {
    		
	        		redLocationScore +=2;
        		
	        	}
			}

		}
    	
    		
		if (myPlayer == 1) {
    		int totScore = redLocationScore - whiteLocationScore;
    		return totScore;
    	}
    	else {
    		int result = whiteLocationScore - redLocationScore;
    		return result;
    	}
   }
    
}
